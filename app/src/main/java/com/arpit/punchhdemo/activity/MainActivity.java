package com.arpit.punchhdemo.activity;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arpit.punchhdemo.R;
import com.arpit.punchhdemo.views.CircleMenu;

public class MainActivity extends AppCompatActivity {

    private ProgressBar seekBar;
    private int counter = 0;
    private int max_val = 14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView company_name = (TextView) findViewById(R.id.company_name);
        TextView receive_text = (TextView) findViewById(R.id.receive_text);
        LinearLayout earn = (LinearLayout) findViewById(R.id.earn);
        final TextView visits_count = (TextView) findViewById(R.id.visits_count);
        seekBar = (ProgressBar) findViewById(R.id.progressWheel);
        final CircleMenu circularView = (CircleMenu) findViewById(R.id.c_menu);
        ProgressBar semi_circle = (ProgressBar) findViewById(R.id.semi_circle);

        semi_circle.setProgress(max_val);   // Main Progress
        semi_circle.setSecondaryProgress(15); // Secondary Progress
        semi_circle.setMax(15); // Maximum Progress

//        Adding 20 items in the circular view
        for (int i = 1; i <= 20; i++) {
            circularView.addMenuItem(false, i);
        }

        seekBar.setProgress(counter);   // Main Progress
        seekBar.setSecondaryProgress(20); // Secondary Progress
        seekBar.setMax(20); // Maximum Progress

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/bastardilla.ttf");
        company_name.setTypeface(custom_font);

        String text = "RECEIVE A  <font color='#B4BE00'>FREE HAIRCUT</font>  AFTER 14TH VISIT";
        if (Build.VERSION.SDK_INT >= 24) {
            receive_text.setText(Html.fromHtml(text, 1)); // for 24 api and more
        } else {
            receive_text.setText(Html.fromHtml(text)); // or for older api
        }

        if (seekBar != null) {
            seekBar.setClickable(false);
            seekBar.setFocusable(false);
            seekBar.setEnabled(false);
            semi_circle.setClickable(false);
            semi_circle.setFocusable(false);
            semi_circle.setEnabled(false);
        }
        visits_count.setText(String.valueOf(counter) + "/" + String.valueOf(max_val));

        earn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (counter >= max_val) {
                    counter = 0;
                    for (int i = 1; i <= 20; i++)
                        circularView.updateMenuItem(false, i);
                }

                if (seekBar != null) {
                    counter++;
                    seekBar.setProgress(counter);
                    circularView.updateMenuItem(true, counter);
                }
//                Redrawing the Circular view after updating the status of the circle
                circularView.invalidate();
                visits_count.setText(String.valueOf(counter) + "/" + String.valueOf(max_val));
            }
        });
    }
}
